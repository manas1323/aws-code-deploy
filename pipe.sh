#!/bin/bash
#
# Perform an AWS CodeDeploy deployment to an existing Application and Deployment Group.
#
# Required globals:
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_DEFAULT_REGION
#   APPLICATION_NAME
#   COMMAND
#   BUNDLE_TYPE
#
# Required (upload)
#   ZIP_FILE
#
# Required (deploy)
#   DEPLOYMENT_GROUP
#
# Optional (common)
#   S3_BUCKET
#   VERSION_LABEL
#   DEBUG
#
# Optional (deploy)
#   FILE_EXISTS_BEHAVIOR
#   IGNORE_APPLICATION_STOP_FAILURES
#   WAIT
#   EXTRA_ARGS
#

# Begin Standard 'imports'
source "$(dirname "$0")/common.sh"

set -e
set -o pipefail


# End standard 'imports'

parse_environment_variables() {
  AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
  AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}
  AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
  APPLICATION_NAME=${APPLICATION_NAME:?'APPLICATION_NAME variable missing.'}
  APPLICATION_NAME_LOWER_CASE=$(echo ${APPLICATION_NAME} | tr '[:upper:]' '[:lower:]')
  S3_BUCKET=${S3_BUCKET:=${APPLICATION_NAME_LOWER_CASE}-codedeploy-deployment}
  VERSION_LABEL=${VERSION_LABEL:=${APPLICATION_NAME_LOWER_CASE}-${BITBUCKET_BUILD_NUMBER}-${BITBUCKET_COMMIT:0:8}}
  COMMAND=${COMMAND:?'COMMAND variable missing.'}
  BUNDLE_TYPE=${BUNDLE_TYPE:='zip'}

  AWS_DEBUG_ARGS=""
  if [[ "${DEBUG}" == "true" ]]; then
      info "Enabling debug mode."
      AWS_DEBUG_ARGS="--debug"
  fi


  if [[ "${COMMAND}" == "upload" ]]; then
    ZIP_FILE=${ZIP_FILE:?'ZIP_FILE variable missing.'}
  elif [[ "${COMMAND}" == "deploy" ]]; then

    WAIT=${WAIT:="true"}
    DEPLOYMENT_GROUP=${DEPLOYMENT_GROUP:?'DEPLOYMENT_GROUP variable missing.'}
    FILE_EXISTS_BEHAVIOUR=${FILE_EXISTS_BEHAVIOR:='DISALLOW'}
    IGNORE_APPLICATION_STOP_FAILURES=${IGNORE_APPLICATION_STOP_FAILURES:="false"}
    APPLICATION_STOP_FAILURES="--no-ignore-application-stop-failures"
    EXTRA_ARGS=${EXTRA_ARGS:=""}
    if [[ "${IGNORE_APPLICATION_STOP_FAILURES}" == "true" ]]; then
      APPLICATION_STOP_FAILURES="--ignore-application-stop-failures"
    fi
  else
      fail "COMMAND must be either 'upload' or 'deploy'"
  fi
}


upload_to_s3() {
    info "Uploading ${ZIP_FILE} to S3."
    run aws s3 cp "${ZIP_FILE}" "s3://${S3_BUCKET}/${VERSION_LABEL}"
    if [[ "${status}" != "0" ]]; then
      fail "Failed to upload ${ZIP_FILE} to S3".
    fi

    info "Registering a revision for the artifact."
    run aws deploy register-application-revision \
      --application-name "${APPLICATION_NAME}" \
      --revision revisionType=S3,s3Location="{bucket=${S3_BUCKET},key=${VERSION_LABEL},bundleType=${BUNDLE_TYPE}}" \
      ${AWS_DEBUG_ARGS}

    if [[ "${status}" == "0" ]]; then
      success "Application uploaded and revision created."
    else
      fail "Failed to register application revision."
    fi
}

wait_for_deploy() {
    if [[ "${WAIT}" == "true" ]]; then
      info "Waiting for deployment to complete."
      run aws deploy wait deployment-successful --deployment-id "${deployment_id}" ${AWS_DEBUG_ARGS}

      if [[ "${status}" == "0" ]]; then
        success "Deployment completed successfully."
      else
        error "Deployment failed. Fetching deployment information..."
        run aws deploy get-deployment --deployment-id "${deployment_id}" ${AWS_DEBUG_ARGS}
        exit 1
      fi
    else
      success "Skip waiting for deployment to complete."
    fi
}

validate_revision() {
  run aws deploy get-application-revision \
    --application-name "${APPLICATION_NAME}" \
    --revision revisionType=S3,s3Location="{bucket=${S3_BUCKET},bundleType=${BUNDLE_TYPE},key=${VERSION_LABEL}}" \
    ${AWS_DEBUG_ARGS}

  if [[ "${status}" != "0" ]]; then
    fail "Failed to fetch revision."
  fi
}


deploy() {
  info "Deploying app from revision."

  validate_revision

  run aws deploy create-deployment \
      --application-name "${APPLICATION_NAME}" \
      --deployment-group "${DEPLOYMENT_GROUP}" \
      --description "Deployed from Bitbucket Pipelines using aws-code-deploy pipe. For details follow the link https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}" \
      --revision revisionType=S3,s3Location="{bucket=${S3_BUCKET},bundleType=${BUNDLE_TYPE},key=${VERSION_LABEL}}" \
      ${APPLICATION_STOP_FAILURES} \
      --file-exists-behavior "${FILE_EXISTS_BEHAVIOUR}" \
      ${EXTRA_ARGS} \
      ${AWS_DEBUG_ARGS}

  if [[ "${status}" == "0" ]]; then
    deployment_id=$(cat "${output_file}" | jq --raw-output '.deploymentId')
    info "Deployment started. Use this link to access the deployment in the AWS console: https://console.aws.amazon.com/codesuite/codedeploy/deployments/${deployment_id}?region=${AWS_DEFAULT_REGION}"
  else
    fail "Failed to create deployment."
  fi

  wait_for_deploy
}

parse_environment_variables

if [[ "${COMMAND}" == "upload" ]]; then
  upload_to_s3
else
  deploy
fi
