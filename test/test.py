import os
import uuid
import shutil
import boto3

from bitbucket_pipes_toolkit.test import PipeTestCase


BASE_NAME = "aws-code-deploy-ci"
S3_BUCKET = f"{BASE_NAME}-codedeploy-deployment"
APPLICATION_NAME = f"{BASE_NAME.upper()}-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
DEPLOYMENT_GROUP = f"{BASE_NAME}-{os.getenv('BITBUCKET_BUILD_NUMBER')}-group"


class DeploySuccessTestCase(PipeTestCase):
    def setUp(self):
        super().setUp()

        self.zip_file_name = f"artifact-{uuid.uuid4().hex}"

        shutil.make_archive(self.zip_file_name, 'zip', 'test/app/')

    def test_should_run_successfully_with_upper_case_application_name(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "COMMAND": "upload",
            "ZIP_FILE": f"{self.zip_file_name}.zip",
            })

        self.assertIn("Application uploaded and revision created", result)

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "DEPLOYMENT_GROUP": DEPLOYMENT_GROUP,
            "COMMAND": "deploy",
            "FILE_EXISTS_BEHAVIOR": "OVERWRITE",
            })

        self.assertIn("Deployment completed successfully", result)

    def tearDown(self):
        os.remove(os.path.join(os.getcwd(), f"{self.zip_file_name}.zip"))


class DeployTestCase(PipeTestCase):

    def setUp(self):
        super().setUp()

        self.zip_file_name = f"artifact-{uuid.uuid4().hex}"

        shutil.make_archive(self.zip_file_name, 'zip', 'test/app/')

    def _get_last_deployment_id(self):
        client = boto3.client('codedeploy', region_name=os.getenv('AWS_DEFAULT_REGION'))
        paginator = client.get_paginator('list_deployments')
        response_iterator = paginator.paginate(
            applicationName=APPLICATION_NAME,
            deploymentGroupName=DEPLOYMENT_GROUP,
            PaginationConfig={
                'MaxItems': 1,
            }
        )
        deployment_id = [page["deployments"][0] for page in response_iterator][0]
        return deployment_id

    def test_link_to_deployment_present(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "upload",
            "ZIP_FILE": f"{self.zip_file_name}.zip",
            })

        self.assertIn("Application uploaded and revision created", result)

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "DEPLOYMENT_GROUP": DEPLOYMENT_GROUP,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "deploy",
            "FILE_EXISTS_BEHAVIOR": "OVERWRITE",
            })

        deployment_id = self._get_last_deployment_id()

        link_to_deployment = f"https://console.aws.amazon.com/codesuite/codedeploy/deployments/{deployment_id}?region={os.getenv('AWS_DEFAULT_REGION')}"
        self.assertIn(link_to_deployment, result)

    def test_description_of_deployment_is_right(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "upload",
            "ZIP_FILE": f"{self.zip_file_name}.zip",
            })

        self.assertIn("Application uploaded and revision created", result)

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "DEPLOYMENT_GROUP": DEPLOYMENT_GROUP,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "deploy",
            "FILE_EXISTS_BEHAVIOR": "OVERWRITE",
            })

        client = boto3.client('codedeploy', region_name=os.getenv('AWS_DEFAULT_REGION'))
        deployment_id = self._get_last_deployment_id()

        response = client.get_deployment(deploymentId=deployment_id)
        deployment_description = response["deploymentInfo"]["description"]
        expected_description = "Deployed from Bitbucket Pipelines using aws-code-deploy pipe."
        self.assertIn(expected_description, deployment_description)

    def test_should_run_succesfully_with_debug_enabled(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "upload",
            "ZIP_FILE": f"{self.zip_file_name}.zip",
            })

        self.assertIn("Application uploaded and revision created", result)

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "DEPLOYMENT_GROUP": DEPLOYMENT_GROUP,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "deploy",
            "FILE_EXISTS_BEHAVIOR": "OVERWRITE",
            })

        self.assertIn("Deployment completed successfully", result)

    def test_should_deploy_with_a_custom_version_label(self):
        label = uuid.uuid4().hex

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "S3_BUCKET": S3_BUCKET,
            "VERSION_LABEL": label,
            "COMMAND": "upload",
            "ZIP_FILE": f"{self.zip_file_name}.zip",
            })

        self.assertIn("Application uploaded and revision created", result)

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "S3_BUCKET": S3_BUCKET,
            "VERSION_LABEL": label,
            "DEPLOYMENT_GROUP": DEPLOYMENT_GROUP,
            "COMMAND": "deploy",
            "FILE_EXISTS_BEHAVIOR": "OVERWRITE",
            })

        client = boto3.client('codedeploy', region_name=os.getenv('AWS_DEFAULT_REGION'))
        revisions = client.list_application_revisions(applicationName=APPLICATION_NAME)

        self.assertIn(label, [item['s3Location']['key'] for item in revisions['revisions']])

        self.assertIn("Deployment completed successfully", result)

    def test_should_deploy_a_revision_and_wait_for_completion(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "upload",
            "ZIP_FILE": f"{self.zip_file_name}.zip",
            })

        self.assertIn("Application uploaded and revision created", result)

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "DEPLOYMENT_GROUP": DEPLOYMENT_GROUP,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "deploy",
            "WAIT": "true",
            "FILE_EXISTS_BEHAVIOR": "OVERWRITE",
            })

        self.assertIn("Deployment completed successfully", result)

    def test_should_deploy_with_extra_args(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "upload",
            "ZIP_FILE": f"{self.zip_file_name}.zip",
            })

        self.assertIn("Application uploaded and revision created", result)

        description = uuid.uuid4().hex

        result = self.run_container(environment={
            "EXTRA_ARGS": f"--description {description}",
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "DEPLOYMENT_GROUP": DEPLOYMENT_GROUP,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "deploy",
            "WAIT": "true",
            "FILE_EXISTS_BEHAVIOR": "OVERWRITE",
            })

        client = boto3.client('codedeploy', region_name=os.getenv('AWS_DEFAULT_REGION'))
        deployments = client.list_deployments(applicationName=APPLICATION_NAME, deploymentGroupName=DEPLOYMENT_GROUP)
        deployment = client.get_deployment(deploymentId=deployments['deployments'][0])
        self.assertEqual(deployment['deploymentInfo']['description'], description)

    def test_should_deploy_a_revision_and_skip_waiting_for_completion(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "upload",
            "IGNORE_APPLICATION_STOP_FAILURES": "true",
            "ZIP_FILE": f"{self.zip_file_name}.zip",
            })

        self.assertIn("Application uploaded and revision created", result)

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "DEPLOYMENT_GROUP": DEPLOYMENT_GROUP,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "deploy",
            "WAIT": "false",
            "FILE_EXISTS_BEHAVIOR": "OVERWRITE",
            })

        self.assertIn("Skip waiting for deployment to complete", result)

        client = boto3.client('codedeploy', region_name=os.getenv('AWS_DEFAULT_REGION'))
        deployments = client.list_deployments(applicationName=APPLICATION_NAME, deploymentGroupName=DEPLOYMENT_GROUP)

        waiter = client.get_waiter('deployment_successful')
        waiter.wait(deploymentId=deployments['deployments'][0])

    def tearDown(self):
        os.remove(os.path.join(os.getcwd(), f"{self.zip_file_name}.zip"))


class BrokenStopScriptTestCase(PipeTestCase):

    def setUp(self):
        super().setUp()

        self.zip_file_name = f"artifact-{uuid.uuid4().hex}"

        shutil.make_archive(self.zip_file_name, 'zip', 'test/app-broken-stop-script/')

    def test_ignore_app_stop_errors_when_deploying_revision(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "upload",
            "ZIP_FILE": f"{self.zip_file_name}.zip",
            })

        self.assertIn("Application uploaded and revision created", result)

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "DEPLOYMENT_GROUP": DEPLOYMENT_GROUP,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "deploy",
            "WAIT": "true",
            "FILE_EXISTS_BEHAVIOR": "OVERWRITE",
            })

        self.assertIn("Deployment completed successfully", result)

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "DEPLOYMENT_GROUP": DEPLOYMENT_GROUP,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "deploy",
            "WAIT": "true",
            "FILE_EXISTS_BEHAVIOR": "OVERWRITE",
            })

        self.assertIn("Deployment failed", result)

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "DEPLOYMENT_GROUP": DEPLOYMENT_GROUP,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "deploy",
            "WAIT": "true",
            "FILE_EXISTS_BEHAVIOR": "OVERWRITE",
            "IGNORE_APPLICATION_STOP_FAILURES": "true"
            })

        self.assertIn("Deployment completed successfully", result)

    def tearDown(self):
        os.remove(os.path.join(os.getcwd(), f"{self.zip_file_name}.zip"))


class UploadTestCase(PipeTestCase):
    def setUp(self):
        super().setUp()

        self.zip_file_name = f"artifact-{uuid.uuid4().hex}"

        shutil.make_archive(self.zip_file_name, 'zip', 'test/app/')

    def test_should_upload_and_create_a_revision_for_a_zip_file_to_a_custom_s3_bucket(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "S3_BUCKET": "bbci-code-deploy-demo",
            "COMMAND": "upload",
            "ZIP_FILE": f"{self.zip_file_name}.zip",
            })

        self.assertIn("Application uploaded and revision created", result)

    def test_should_upload_and_create_a_revision_for_a_zip_file(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv('AWS_ACCESS_KEY_ID'),
            "AWS_SECRET_ACCESS_KEY": os.getenv('AWS_SECRET_ACCESS_KEY'),
            "AWS_DEFAULT_REGION": os.getenv('AWS_DEFAULT_REGION'),
            "APPLICATION_NAME": APPLICATION_NAME,
            "S3_BUCKET": S3_BUCKET,
            "COMMAND": "upload",
            "ZIP_FILE": f"{self.zip_file_name}.zip",
            })

        self.assertIn("Application uploaded and revision created", result)

    def tearDown(self):
        os.remove(os.path.join(os.getcwd(), f"{self.zip_file_name}.zip"))
