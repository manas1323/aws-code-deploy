AWSTemplateFormatVersion: 2010-09-09

Parameters:
  ApplicationName:
    Type: String
  DeploymentGroupName:
    Type: String
  BucketName:
    Type: String

Resources:
  S3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Ref BucketName
      AccessControl: BucketOwnerFullControl
      Tags:
        - Key: Application
          Value: !Ref ApplicationName
        - Key: Owner
          Value: bitbucket-pipelines
        - Key: Team
          Value: bitbucket-pipes
  vpc:
    Type: 'AWS::EC2::VPC'
    Properties:
      CidrBlock: 10.0.0.0/16
      EnableDnsSupport: 'true'
      EnableDnsHostnames: 'true'
      InstanceTenancy: default
      Tags:
        - Key: Application
          Value: !Ref ApplicationName
        - Key: Owner
          Value: bitbucket-pipelines
        - Key: Team
          Value: bitbucket-pipes
        - Key: Name
          Value: !Ref ApplicationName
  subnet:
    Type: 'AWS::EC2::Subnet'
    Properties:
      AvailabilityZone: ap-southeast-2a
      CidrBlock: 10.0.0.0/24
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Application
          Value: !Ref ApplicationName
        - Key: Owner
          Value: bitbucket-pipelines
        - Key: Team
          Value: bitbucket-pipes
      VpcId: !Ref vpc
  codeDeployInstance:
    Type: 'AWS::EC2::Instance'
    Properties:
      AvailabilityZone: ap-southeast-2a
      IamInstanceProfile: !Ref codeDeployEc2InstanceProfile
      ImageId: ami-00e17d1165b9dd3ec
      InstanceType: t2.micro
      KeyName: azhukov-codedeploy
      Monitoring: 'false'
      SecurityGroupIds:
        - !Ref codeDeployEc2InstanceSecurityGroup
      SubnetId: !Ref subnet
      Tags:
        - Key: Application
          Value: !Ref ApplicationName
        - Key: Owner
          Value: bitbucket-pipelines
        - Key: Team
          Value: bitbucket-pipes
        - Key: Name
          Value: !Ref ApplicationName
      UserData: 
        Fn::Base64: !Sub |
              #!/bin/bash
              yum update
              yum install -y java-1.8.0-openjdk
              yum install -y ruby
              yum install -y wget
              cd /home/ec2-user
              wget https://aws-codedeploy-ap-southeast-2.s3.amazonaws.com/latest/install
              chmod +x ./install
              sudo ./install auto
    DependsOn: codeDeployEc2InstanceSecurityGroup
  codeDeployApplication:
    Type: 'AWS::CodeDeploy::Application'
    Properties:
      ApplicationName: !Ref ApplicationName
      ComputePlatform: Server
  codeDeployServiceRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - codedeploy.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole'
  codeDeployIgw:
    Type: 'AWS::EC2::InternetGateway'
    Properties:
      Tags:
        - Key: Application
          Value: !Ref ApplicationName
        - Key: Name
          Value: azhukov-codedeploy-igw
  codeDeployAttachIgw:
    Type: 'AWS::EC2::VPCGatewayAttachment'
    Properties:
      InternetGatewayId: !Ref codeDeployIgw
      VpcId: !Ref vpc

  RouteTable:
    Type: "AWS::EC2::RouteTable"
    Properties:
      VpcId: !Ref vpc
      Tags:
        - Key: Application
          Value: !Ref ApplicationName
        - Key: Name
          Value: azhukov-codedeploy-route-table

  SubRtAss:
    Type: "AWS::EC2::SubnetRouteTableAssociation"
    Properties:
      RouteTableId: !Ref RouteTable
      SubnetId: !Ref subnet

  DefaultRoute:
    Type: "AWS::EC2::Route"
    Properties:
      DestinationCidrBlock: "0.0.0.0/0"
      GatewayId: !Ref codeDeployIgw
      RouteTableId: !Ref RouteTable
  codeDeployDeploymentGroup:
    Type: 'AWS::CodeDeploy::DeploymentGroup'
    DependsOn: codeDeployInstance
    Properties:
      ApplicationName: !Ref codeDeployApplication
      DeploymentConfigName: CodeDeployDefault.OneAtATime
      DeploymentGroupName: !Ref DeploymentGroupName
      DeploymentStyle:
        DeploymentType: IN_PLACE
        DeploymentOption: WITHOUT_TRAFFIC_CONTROL
      Ec2TagFilters:
        - Key: Application
          Value: !Ref ApplicationName
          Type: KEY_AND_VALUE
      ServiceRoleArn: !GetAtt 
        - codeDeployServiceRole
        - Arn
  codeDeployEc2InstanceProfile:
    Type: 'AWS::IAM::InstanceProfile'
    Properties:
      Path: /
      Roles:
        - !Ref codeDeployEc2InstanceRole
    DependsOn: codeDeployEc2InstanceRole
  codeDeployEc2InstanceRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      Policies:
        - PolicyName: root
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - 's3:Get*'
                  - 's3:List*'
                Resource:
                  - arn:aws:s3:::aws-code-deploy-ci-codedeploy-deployment
                  - arn:aws:s3:::aws-code-deploy-ci-codedeploy-deployment/*
                  - !GetAtt S3Bucket.Arn
                  - !Sub 'arn:aws:s3:::${S3Bucket}/*'
  codeDeployEc2InstanceSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      VpcId: !Ref vpc
      GroupDescription: Security Group for CodeDeploy instance
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 8080
          ToPort: 8080
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: 22
          ToPort: 22
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: 443
          ToPort: 443
          CidrIp: 0.0.0.0/0

      SecurityGroupEgress:
        - IpProtocol: tcp
          FromPort: 0
          ToPort: 65535
          CidrIp: 0.0.0.0/0
